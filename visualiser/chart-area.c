/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "chart-area.h"
#include "log-parser.h"

static void mlv_chart_area_dispose (GObject *obj);
static void mlv_chart_area_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void mlv_chart_area_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);

static void parse_complete_cb (MlvLogParser *parser, MlvChartArea *self);

static void mlv_chart_area_destroy (GtkWidget *widget);
static void mlv_chart_area_map (GtkWidget *widget);
static void mlv_chart_area_realize (GtkWidget *widget);
static void mlv_chart_area_unrealize (GtkWidget *widget);
static void mlv_chart_area_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static gint mlv_chart_area_draw (GtkWidget *widget, cairo_t *cr);
static void mlv_chart_area_get_preferred_height (GtkWidget *widget, gint *minimum, gint *natural);
static void mlv_chart_area_get_preferred_width (GtkWidget *widget, gint *minimum, gint *natural);

static void set_adjustment_value (MlvChartArea *self, GtkOrientation orientation);
static void set_adjustment (MlvChartArea *self, GtkOrientation orientation, GtkAdjustment *adjustment);
static void disconnect_adjustment (MlvChartArea *self, GtkOrientation orientation);
static void set_scroll_policy (MlvChartArea *self, GtkOrientation orientation, GtkScrollablePolicy policy);

struct _MlvChartAreaPrivate {
	MlvLogParser *parser;
	gulong parser_parse_complete_id;

	GdkWindow *bin_window;

	GtkAdjustment *adjustment[2]; /* horizontal, vertical */
	GtkScrollablePolicy scroll_policy[2]; /* horizontal, vertical */
	guint xoffset;
	guint yoffset;
};

typedef enum {
	PROP_PARSER = 1,
	PROP_HADJUSTMENT,
	PROP_VADJUSTMENT,
	PROP_HSCROLL_POLICY,
	PROP_VSCROLL_POLICY,
} MlvChartAreaProperties;

G_DEFINE_TYPE_EXTENDED (MlvChartArea, mlv_chart_area, GTK_TYPE_WIDGET, 0,
	                      G_IMPLEMENT_INTERFACE (GTK_TYPE_SCROLLABLE, NULL))

static void
mlv_chart_area_class_init (MlvChartAreaClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *gtk_widget_class = GTK_WIDGET_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MlvChartAreaPrivate));

	gobject_class->dispose = mlv_chart_area_dispose;
	gobject_class->get_property = mlv_chart_area_get_property;
	gobject_class->set_property = mlv_chart_area_set_property;

	gtk_widget_class->destroy = mlv_chart_area_destroy;

	gtk_widget_class->map = mlv_chart_area_map;
	gtk_widget_class->realize = mlv_chart_area_realize;
	gtk_widget_class->unrealize = mlv_chart_area_unrealize;
	gtk_widget_class->size_allocate = mlv_chart_area_size_allocate;
	/*gtk_widget_class->state_changed = mlv_chart_area_state_changed;
	gtk_widget_class->state_flags_changed = mlv_chart_area_state_flags_changed;
	gtk_widget_class->parent_set = mlv_chart_area_parent_set;
	gtk_widget_class->hierarchy_changed = mlv_chart_area_hierarchy_changed;
	gtk_widget_class->style_set = mlv_chart_area_style_set;
	gtk_widget_class->direction_changed = mlv_chart_area_direction_changed;*/
	gtk_widget_class->draw = mlv_chart_area_draw;
	gtk_widget_class->get_preferred_height = mlv_chart_area_get_preferred_height;
	gtk_widget_class->get_preferred_width = mlv_chart_area_get_preferred_width;
	/*gtk_widget_class->style_updated = mlv_chart_area_style_updated;*/

	/* GtkScrollable implementation */
	g_object_class_override_property (gobject_class, PROP_HADJUSTMENT, "hadjustment");
	g_object_class_override_property (gobject_class, PROP_VADJUSTMENT, "vadjustment");
	g_object_class_override_property (gobject_class, PROP_HSCROLL_POLICY, "hscroll-policy");
	g_object_class_override_property (gobject_class, PROP_VSCROLL_POLICY, "vscroll-policy");

	/**
	 * MlvChartArea:parser:
	 *
	 * TODO
	 */
	g_object_class_install_property (gobject_class, PROP_PARSER,
	                                 g_param_spec_object ("parser",
	                                                      "Parser", "Log file parser TODO.",
	                                                      MLV_TYPE_LOG_PARSER,
	                                                      G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
mlv_chart_area_init (MlvChartArea *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, MLV_TYPE_CHART_AREA, MlvChartAreaPrivate);

	set_adjustment (self, GTK_ORIENTATION_HORIZONTAL, NULL);
	set_adjustment (self, GTK_ORIENTATION_VERTICAL, NULL);
}

static void
mlv_chart_area_destroy (GtkWidget *widget)
{
	MlvChartArea *self = MLV_CHART_AREA (widget);

	disconnect_adjustment (self, GTK_ORIENTATION_HORIZONTAL);
	disconnect_adjustment (self, GTK_ORIENTATION_VERTICAL);

	/* Chain up to the parent class */
	GTK_WIDGET_CLASS (mlv_chart_area_parent_class)->destroy (widget);
}

static void
mlv_chart_area_dispose (GObject *obj)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (obj)->priv;

	if (priv->parser_parse_complete_id != 0) {
		g_signal_handler_disconnect (priv->parser, priv->parser_parse_complete_id);
		priv->parser_parse_complete_id = 0;
	}
	g_clear_object (&priv->parser);

	G_OBJECT_CLASS (mlv_chart_area_parent_class)->dispose (obj);
}

static void
mlv_chart_area_get_property (GObject *object, MlvChartAreaProperties property_id, GValue *value, GParamSpec *pspec)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (object)->priv;

	switch (property_id) {
		case PROP_PARSER:
			g_value_set_object (value, priv->parser);
			break;
		case PROP_HADJUSTMENT:
			g_value_set_object (value, priv->adjustment[GTK_ORIENTATION_HORIZONTAL]);
			break;
		case PROP_VADJUSTMENT:
			g_value_set_object (value, priv->adjustment[GTK_ORIENTATION_VERTICAL]);
			break;
		case PROP_HSCROLL_POLICY:
			g_value_set_enum (value, priv->scroll_policy[GTK_ORIENTATION_HORIZONTAL]);
			break;
		case PROP_VSCROLL_POLICY:
			g_value_set_enum (value, priv->scroll_policy[GTK_ORIENTATION_VERTICAL]);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
mlv_chart_area_set_property (GObject *object, MlvChartAreaProperties property_id, const GValue *value, GParamSpec *pspec)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (object)->priv;

	switch (property_id) {
		case PROP_PARSER:
			/* Construct only. */
			g_assert (priv->parser == NULL);
			priv->parser = g_value_dup_object (value);
			priv->parser_parse_complete_id = g_signal_connect (priv->parser, "parse-complete", (GCallback) parse_complete_cb, object);
			break;
		case PROP_HADJUSTMENT:
			set_adjustment (MLV_CHART_AREA (object), GTK_ORIENTATION_HORIZONTAL, g_value_get_object (value));
			break;
		case PROP_VADJUSTMENT:
			set_adjustment (MLV_CHART_AREA (object), GTK_ORIENTATION_VERTICAL, g_value_get_object (value));
			break;
		case PROP_HSCROLL_POLICY:
			set_scroll_policy (MLV_CHART_AREA (object), GTK_ORIENTATION_HORIZONTAL, g_value_get_enum (value));
			break;
		case PROP_VSCROLL_POLICY:
			set_scroll_policy (MLV_CHART_AREA (object), GTK_ORIENTATION_VERTICAL, g_value_get_enum (value));
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
parse_complete_cb (MlvLogParser *parser, MlvChartArea *self)
{
	gtk_widget_queue_resize (GTK_WIDGET (self)); /* also queues a redraw */
}

static void
mlv_chart_area_map (GtkWidget *widget)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (widget)->priv;

	gtk_widget_set_mapped (widget, TRUE);

	gdk_window_show (priv->bin_window);
	gdk_window_show (gtk_widget_get_window (widget));
}

static void
mlv_chart_area_realize (GtkWidget *widget)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (widget)->priv;
	GtkAllocation allocation;
	GdkWindow *window;
	GdkWindowAttr attributes;
	gint attributes_mask;
	GtkStyleContext *style_context;
	GtkCssProvider *provider;

	gtk_widget_set_realized (widget, TRUE);

	/* Unscrolled window. */
	gtk_widget_get_allocation (widget, &allocation);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = allocation.x;
	attributes.y = allocation.y;
	attributes.width = allocation.width;
	attributes.height = allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.event_mask = GDK_VISIBILITY_NOTIFY_MASK;

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

	window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
	gtk_widget_set_window (widget, window);
	gdk_window_set_user_data (window, widget);

	/* Scrolled window. */
	gtk_widget_get_allocation (widget, &allocation);

	attributes.x = - gtk_adjustment_get_value (priv->adjustment[GTK_ORIENTATION_HORIZONTAL]),
	attributes.y = - gtk_adjustment_get_value (priv->adjustment[GTK_ORIENTATION_VERTICAL]);
	attributes.width = allocation.width;
	attributes.height = allocation.height;
	attributes.event_mask = GDK_EXPOSURE_MASK | GDK_SCROLL_MASK | GDK_SMOOTH_SCROLL_MASK | gtk_widget_get_events (widget);

	priv->bin_window = gdk_window_new (window, &attributes, attributes_mask);
	gdk_window_set_user_data (priv->bin_window, widget);

	/* Style context and fallback style provider. */
	style_context = gtk_widget_get_style_context (widget);
	gtk_style_context_set_background (style_context, priv->bin_window);

	/* TODO: Push this out to a file. */
	provider = gtk_css_provider_get_default ();
	gtk_css_provider_load_from_data (provider,
		"MlvChartArea thread {"
			"color: red;"
		"}"
		"MlvChartArea event {"
			"border-color: red;"
			"background-color: red;" /* TODO: doesn't seem to be working */
		"}"
		"MlvChartArea time-tick {"
			"font: Sans 7;"
		"}", -1, NULL);
	gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_FALLBACK);
}

static void
mlv_chart_area_unrealize (GtkWidget *widget)
{
	MlvChartAreaPrivate *priv = MLV_CHART_AREA (widget)->priv;

	gdk_window_set_user_data (priv->bin_window, NULL);
	gdk_window_destroy (priv->bin_window);
	priv->bin_window = NULL;

	GTK_WIDGET_CLASS (mlv_chart_area_parent_class)->unrealize (widget);
}

static void
mlv_chart_area_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	MlvChartArea *self = MLV_CHART_AREA (widget);
	MlvChartAreaPrivate *priv = self->priv;

	gtk_widget_set_allocation (widget, allocation);

	if (gtk_widget_get_realized (widget)) {
		gdk_window_move_resize (gtk_widget_get_window (widget), allocation->x, allocation->y, allocation->width, allocation->height);

		gdk_window_resize (priv->bin_window, allocation->width, allocation->height);
	}

	/* Update adjustments. */
	set_adjustment_value (self, GTK_ORIENTATION_HORIZONTAL);
	set_adjustment_value (self, GTK_ORIENTATION_VERTICAL);
}

/* TODO: Make these CSS controlled */
#define THREAD_SPACING 100 /* pixels */
#define THREAD_TOP_MARGIN 20 /* pixels */
#define THREAD_BOTTOM_MARGIN 20 /* pixels */
#define THREAD_LEFT_MARGIN 50 /* pixels */
#define THREAD_RIGHT_MARGIN 50 /* pixels */
#define SCALE 50 /* pixels per unit timestamp */
#define EVENT_RADIUS 5 /* pixels */
#define TIME_TICK_WIDTH 15 /* pixels */
#define TIME_LABEL_WIDTH 100 /* pixels */
#define TIME_LABEL_RIGHT_PADDING 5 /* pixels */
#define MAIN_CONTEXT_RADIUS 5 /* pixels */

static GtkRegionFlags
calculate_region_flags_for_thread (guint thread_id /* 0-based */, guint num_threads)
{
	return ((thread_id % 2 == 0) ? GTK_REGION_EVEN : GTK_REGION_ODD) |
	       ((thread_id == 0) ? GTK_REGION_FIRST : 0) |
	       ((thread_id == num_threads - 1) ? GTK_REGION_LAST : 0);
}

static GtkRegionFlags
calculate_region_flags_for_timestamp (gint64 timestamp, gint64 min_timestamp, gint64 max_timestamp)
{
	return ((timestamp % 2 == 0) ? GTK_REGION_EVEN : GTK_REGION_ODD) |
	       ((timestamp == min_timestamp) ? GTK_REGION_FIRST : 0) |
	       ((timestamp == max_timestamp) ? GTK_REGION_LAST : 0);
}

static void
draw_event_circle (cairo_t *cr, GtkStyleContext *context, gint64 min_timestamp, guint num_threads, MlvLogEvent *event)
{
	GdkRGBA border_color, background_color;

	cairo_save (cr);
	gtk_style_context_save (context);
	gtk_style_context_add_region (context, "event", calculate_region_flags_for_thread (event->thread_id, num_threads));

	gtk_style_context_get_border_color (context, GTK_STATE_FLAG_NORMAL, &border_color);
	gtk_style_context_get_background_color (context, GTK_STATE_FLAG_NORMAL, &background_color);

	cairo_arc (cr,
	           THREAD_SPACING * event->thread_id,
	           THREAD_TOP_MARGIN + SCALE * (event->timestamp - min_timestamp),
	           EVENT_RADIUS, 0, 2 * M_PI);

	gdk_cairo_set_source_rgba (cr, &background_color);
	cairo_fill_preserve (cr);

	gdk_cairo_set_source_rgba (cr, &border_color);
	cairo_stroke (cr);

	gtk_style_context_restore (context);
	cairo_restore (cr);
}

static gint
event_sort_cb (const MlvLogEvent *event_a, const MlvLogEvent *event_b, gpointer user_data)
{
	return event_a->timestamp - event_b->timestamp;
}

/* TODO */
#include <stdlib.h>

static gint
mlv_chart_area_draw (GtkWidget *widget, cairo_t *cr)
{
	MlvChartArea *self = MLV_CHART_AREA (widget);
	MlvChartAreaPrivate *priv = self->priv;
	GtkStyleContext *context;
	GtkAllocation allocation;

	context = gtk_widget_get_style_context (widget);
	gtk_widget_get_allocation (widget, &allocation);

	if (gtk_cairo_should_draw_window (cr, priv->bin_window)) {
		guint i;
		gint64 j, min_timestamp, max_timestamp, screen_min_timestamp, screen_max_timestamp;
		GArray/*<MlvLogThread>*/ *threads;
		GSequence/*<MlvLogEvent>*/ *events;
		GSequenceIter/*<MlvLogEvent>*/ *events_iter, *events_end_iter;
		MlvLogEvent fake_event;

		/* Transform to the window coordinates, to allow for scrolling. */
		cairo_save (cr);
		gtk_cairo_transform_to_window (cr, widget, priv->bin_window);
		cairo_translate (cr, -priv->xoffset, -priv->yoffset);

		/* Add the top margin. */
		cairo_translate (cr, 0, THREAD_TOP_MARGIN);

		min_timestamp = mlv_log_parser_get_min_timestamp (priv->parser);
		max_timestamp = mlv_log_parser_get_max_timestamp (priv->parser);
		threads = mlv_log_parser_get_threads (priv->parser);
		events = mlv_log_parser_get_events (priv->parser);

		/* +-2 are to allow for rounding errors */
		#define TIMESTAMP_FUZZ 2
		screen_min_timestamp = CLAMP (min_timestamp + gtk_adjustment_get_value (priv->adjustment[GTK_ORIENTATION_VERTICAL]) / SCALE - TIMESTAMP_FUZZ, min_timestamp, max_timestamp);
		screen_max_timestamp = CLAMP (screen_min_timestamp + allocation.height / SCALE + 2 * TIMESTAMP_FUZZ, min_timestamp, max_timestamp);

		/* TODO: Debug */
		cairo_set_source_rgba (cr, ((gdouble) rand ()) / RAND_MAX, ((gdouble) rand ()) / RAND_MAX, ((gdouble) rand ()) / RAND_MAX, 0.5);
		cairo_paint_with_alpha (cr, 0.5);

		/* Draw the time scale down the left of the widget. */
		for (j = screen_min_timestamp; j < screen_max_timestamp; j++) {
			PangoLayout *layout;
			gint layout_width, layout_height;
			gchar *tick_text;

			gtk_style_context_save (context);
			gtk_style_context_add_region (context, "time-tick", calculate_region_flags_for_timestamp (j, min_timestamp, max_timestamp));

			/* Tick */
			gtk_render_line (context, cr,
			                 TIME_LABEL_WIDTH + TIME_LABEL_RIGHT_PADDING, SCALE * (j - min_timestamp),
			                 TIME_LABEL_WIDTH + TIME_LABEL_RIGHT_PADDING + TIME_TICK_WIDTH, SCALE * (j - min_timestamp));

			/* Label */
			layout = pango_cairo_create_layout (cr);
			pango_layout_set_font_description (layout, gtk_style_context_get_font (context, GTK_STATE_FLAG_NORMAL));

			tick_text = g_strdup_printf ("%lu", j);
			pango_layout_set_text (layout, tick_text, -1);
			g_free (tick_text);

			pango_layout_get_size (layout, &layout_width, &layout_height);
			cairo_set_source_rgb (cr, 0, 0, 0); /* TODO */
			cairo_move_to (cr,
			               TIME_LABEL_WIDTH - ((gdouble) layout_width / PANGO_SCALE),
			               SCALE * (j - min_timestamp) - ((gdouble) layout_height / PANGO_SCALE) / 2);
			pango_cairo_show_layout (cr, layout);

			g_object_unref (layout);

			gtk_style_context_restore (context);
		}

		/* Add the left margin. */
		cairo_translate (cr, TIME_LABEL_WIDTH + TIME_LABEL_RIGHT_PADDING + TIME_TICK_WIDTH + THREAD_LEFT_MARGIN, 0);

		/* Draw each thread. */
		for (i = 0; i < threads->len; i++) {
			PangoLayout *layout;
			gint layout_width, layout_height;
			gchar *thread_text;

			gtk_style_context_save (context);
			gtk_style_context_add_region (context, "thread", calculate_region_flags_for_thread (i, threads->len));

			/* Draw the thread label. TODO: Pin it to the screen so it doesn't scroll? */
			layout = pango_cairo_create_layout (cr);

			thread_text = g_strdup_printf (_("Thread %d"), g_array_index (threads, MlvLogThread, i).thread_pid);
			pango_layout_set_text (layout, thread_text, -1);
			g_free (thread_text);

			pango_layout_get_size (layout, &layout_width, &layout_height);
			cairo_set_source_rgb (cr, 0, 0, 0); /* TODO */
			cairo_move_to (cr,
			               THREAD_SPACING * i - ((gdouble) layout_width / PANGO_SCALE) / 2,
			               ((gdouble) layout_height / PANGO_SCALE));
			pango_cairo_show_layout (cr, layout);

			g_object_unref (layout);

			/* Draw the thread line. */
			gtk_render_line (context, cr,
			                 THREAD_SPACING * i, 0,
			                 THREAD_SPACING * i, SCALE * (max_timestamp - min_timestamp));

			gtk_style_context_restore (context);
		}

		/* Draw the events. */
		fake_event.timestamp = screen_min_timestamp - 1 /* to avoid off-by-one errors */;
		events_iter = g_sequence_search (events, &fake_event, (GCompareDataFunc) event_sort_cb, NULL);
		events_end_iter = g_sequence_get_end_iter (events);

		g_message ("\n\n\n");

		while (events_iter != events_end_iter) {
			MlvLogEvent *event = (MlvLogEvent*) g_sequence_get (events_iter);

			g_message ("outer event %p, prev %p, next %p", event, event->prev, event->next);
			g_message ("min %lu, max %lu, event %lu", screen_min_timestamp, screen_max_timestamp, event->timestamp);

			/* If this event is part-way through a chain (i.e. in any position except at the head), work backwards until we reach the head,
			 * then render that. This simplifies the rendering code by allowing all rendering of chains to proceed from head to tail, without
			 * having to handle the case where only one event part-way through the chain is visible and thus needs to render backwards up
			 * the chain as well as forwards down the chain.
			 *
			 * In order to only process each chain once, we only proceed upwards to the head if this is the first event in the chain which
			 * is currently visible. Otherwise, we bail. */
			if (event->prev != NULL) {
				g_assert (event->timestamp >= screen_min_timestamp);

				g_message ("prev timestamp %lu", event->prev->timestamp);

				if (event->prev->timestamp < screen_min_timestamp) {
					/* Work back to the head of the chain. */
					do {
						/* We require this to be a linear chain. */
						g_assert (event->prev->next == event);
						event = event->prev;
					} while (event->prev != NULL);

					g_message ("worked back to event %p, prev %p, next %p", event, event->prev, event->next);
				} else {
					/* Bail! */
					goto next_event_chain;
				}
			}

			g_message ("starting from event %p", event);

			for (; event != NULL; event = event->next) {
				g_message ("drawing event %p", event);
				/* Stop iterating if we've gone off the bottom of the screen. */
				/* TODO: This means we won't draw (e.g.) arrows which start off the top of the screen and finish off the bottom. */
				if (event->timestamp > screen_max_timestamp) {
					goto break_event_chain;
				}

				/* Render the event. */
				switch (event->event_type) {
					case MLV_LOG_EVENT_MAIN_CONTEXT_NEW:
					case MLV_LOG_EVENT_MAIN_CONTEXT_DEFAULT:
					case MLV_LOG_EVENT_MAIN_LOOP_QUIT:
						break;
					case MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE:
						/* Draw enclosing lines around the relevant thread until the GMainContext was released. */
						if (event->next != NULL && event->next->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE) {
							gtk_render_line (context, cr,
							                 THREAD_SPACING * event->thread_id - MAIN_CONTEXT_RADIUS, SCALE * (event->timestamp - min_timestamp),
							                 THREAD_SPACING * event->thread_id - MAIN_CONTEXT_RADIUS, SCALE * (event->next->timestamp - min_timestamp));
							gtk_render_line (context, cr,
							                 THREAD_SPACING * event->thread_id + MAIN_CONTEXT_RADIUS, SCALE * (event->timestamp - min_timestamp),
							                 THREAD_SPACING * event->thread_id + MAIN_CONTEXT_RADIUS, SCALE * (event->next->timestamp - min_timestamp));
						}

						break;
					case MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE:
						if (event->next != NULL) {
							double dashes[] = { 1.0, 4.0 }; /* dotted line */

							if (event->next->thread_id != event->thread_id) {
								/* Draw an arrow from the old thread to the new thread. */
								/* TODO: tidy this up */
								gtk_render_line (context, cr,
								                 THREAD_SPACING * event->thread_id, SCALE * (event->timestamp - min_timestamp),
								                 THREAD_SPACING * event->next->thread_id, SCALE * (event->timestamp - min_timestamp));
							}

							/* Draw enclosing dotted lines around the new thread from the old event to the new event. */
							cairo_save (cr);

							cairo_set_dash (cr, dashes, G_N_ELEMENTS (dashes), 0.0);
							cairo_set_line_cap (cr, CAIRO_LINE_CAP_SQUARE);
							cairo_set_line_width (cr, 1.0);

							cairo_move_to (cr, THREAD_SPACING * event->next->thread_id - MAIN_CONTEXT_RADIUS, SCALE * (event->timestamp - min_timestamp));
							cairo_line_to (cr, THREAD_SPACING * event->next->thread_id - MAIN_CONTEXT_RADIUS, SCALE * (event->next->timestamp - min_timestamp));

							cairo_move_to (cr, THREAD_SPACING * event->next->thread_id + MAIN_CONTEXT_RADIUS, SCALE * (event->timestamp - min_timestamp));
							cairo_line_to (cr, THREAD_SPACING * event->next->thread_id + MAIN_CONTEXT_RADIUS, SCALE * (event->next->timestamp - min_timestamp));

							cairo_stroke (cr);

							cairo_restore (cr);
						}

						break;
					case MLV_LOG_EVENT_SOURCE_ATTACH:
						/* Draw an event circle and an arrow to the thread which currently owns the relevant GMainContext. */
						draw_event_circle (cr, context, min_timestamp, threads->len, event);

						/* TODO: get context thread ID from this GSource's chain? */

						gtk_render_line (context, cr,
						                 THREAD_SPACING * event->thread_id, SCALE * (event->timestamp - min_timestamp),
						                 THREAD_SPACING * 1, SCALE * (event->timestamp - min_timestamp));

						break;
					case MLV_LOG_EVENT_MAIN_LOOP_NEW:
						draw_event_circle (cr, context, min_timestamp, threads->len, event);
						break;
					case MLV_LOG_EVENT_IDLE_ADD:
					case MLV_LOG_EVENT_TIMEOUT_ADD:
						draw_event_circle (cr, context, min_timestamp, threads->len, event);
						break;
					case MLV_LOG_EVENT_IDLE_DISPATCH:
						draw_event_circle (cr, context, min_timestamp, threads->len, event);

						/* TODO:
						 * Need a post-parsing step in log-parser which walks backwards through the event list and builds chains of
						 * related events using MlvLogEvent->[next|prev]. We can then use those chains to re-implement the code below,
						 * and also implement rendering of GMainContext acquire/release events. */

						break;
					case MLV_LOG_EVENT_TIMEOUT_DISPATCH:
						draw_event_circle (cr, context, min_timestamp, threads->len, event);

						break;
					case MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY:
						break;
					default:
						g_assert_not_reached ();
				}
			}

next_event_chain:
			/* Continue iterating. */
			events_iter = g_sequence_iter_next (events_iter);
		}

break_event_chain:
		/* Restore back to widget coordinates. */
		cairo_restore (cr);
	}

	return FALSE;
}

static void
calculate_size_request (MlvChartArea *self, GtkRequisition *requisition)
{
	GArray/*<MlvLogThread>*/ *threads;

	/* TODO: probably need some caching here */
	threads = mlv_log_parser_get_threads (self->priv->parser);
	requisition->width = THREAD_LEFT_MARGIN + threads->len * THREAD_SPACING + THREAD_RIGHT_MARGIN;
	requisition->height = THREAD_TOP_MARGIN +
	                      SCALE * (mlv_log_parser_get_max_timestamp (self->priv->parser) - mlv_log_parser_get_min_timestamp (self->priv->parser)) +
	                      THREAD_BOTTOM_MARGIN;
}

static void
mlv_chart_area_get_preferred_height (GtkWidget *widget, gint *minimum, gint *natural)
{
	GtkRequisition requisition;

	calculate_size_request (MLV_CHART_AREA (widget), &requisition);
	*minimum = *natural = requisition.height;
}

static void
mlv_chart_area_get_preferred_width (GtkWidget *widget, gint *minimum, gint *natural)
{
	GtkRequisition requisition;

	calculate_size_request (MLV_CHART_AREA (widget), &requisition);
	*minimum = *natural = requisition.width;
}

static void
set_adjustment_value (MlvChartArea *self, GtkOrientation orientation)
{
	MlvChartAreaPrivate *priv = self->priv;
	gint screen_value, requisition_value;
	gdouble old_value;
	gdouble new_value;
	gdouble new_upper;
	GtkAllocation allocation;
	GtkRequisition requisition;

	/* Don't know our dimensions until we're realised. */
	if (gtk_widget_get_realized (GTK_WIDGET (self)) == FALSE) {
		return;
	}

	gtk_widget_get_allocation (GTK_WIDGET (self), &allocation);
	calculate_size_request (self, &requisition);

	switch (orientation) {
		case GTK_ORIENTATION_HORIZONTAL:
			screen_value = allocation.width;
			requisition_value = requisition.width;
			break;
		case GTK_ORIENTATION_VERTICAL:
			screen_value = allocation.height;
			requisition_value = requisition.height;
			break;
		default:
			g_assert_not_reached ();
	}

	old_value = gtk_adjustment_get_value (priv->adjustment[orientation]);
	new_upper = MAX (screen_value, requisition_value);

	g_object_set (priv->adjustment[orientation],
	              "lower", 0.0,
	              "upper", new_upper,
	              "page-size", (gdouble) screen_value,
	              "step-increment", screen_value * 0.1,
	              "page-increment", screen_value * 0.9,
	              NULL);

	new_value = CLAMP (old_value, 0, new_upper - screen_value);
	if (new_value != old_value) {
		gtk_adjustment_set_value (priv->adjustment[orientation], new_value);
	}
}

static void
adjustment_value_changed_cb (GtkAdjustment *adjustment, MlvChartArea *self)
{
	MlvChartAreaPrivate *priv = self->priv;
	gint dx = 0, dy = 0;

	if (adjustment == priv->adjustment[GTK_ORIENTATION_HORIZONTAL]) {
		dx = priv->xoffset - (gint) gtk_adjustment_get_value (adjustment);
		priv->xoffset = gtk_adjustment_get_value (adjustment);
	} else if (adjustment == priv->adjustment[GTK_ORIENTATION_VERTICAL]) {
		dy = priv->yoffset - (gint) gtk_adjustment_get_value (adjustment);
		priv->yoffset = gtk_adjustment_get_value (adjustment);
	}

	if ((dx != 0 || dy != 0) &&
	    gtk_widget_get_realized (GTK_WIDGET (self))) {
		gdk_window_scroll (priv->bin_window, dx, dy);
	}
}

static void
set_adjustment (MlvChartArea *self, GtkOrientation orientation, GtkAdjustment *adjustment)
{
	MlvChartAreaPrivate *priv = self->priv;

	if (adjustment != NULL && priv->adjustment[orientation] == adjustment) {
		return;
	}

	if (adjustment == NULL) {
		adjustment = gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}

	disconnect_adjustment (self, orientation);
	priv->adjustment[orientation] = adjustment;
	g_object_ref_sink (adjustment);

	set_adjustment_value (self, orientation);

	g_signal_connect (adjustment, "value-changed", (GCallback) adjustment_value_changed_cb, self);
	adjustment_value_changed_cb (adjustment, self);
}

static void
disconnect_adjustment (MlvChartArea *self, GtkOrientation orientation)
{
	MlvChartAreaPrivate *priv = self->priv;

	if (priv->adjustment[orientation] != NULL) {
		g_signal_handlers_disconnect_by_func (priv->adjustment[orientation], adjustment_value_changed_cb, self);
		g_clear_object (&priv->adjustment[orientation]);
	}
}

static void
set_scroll_policy (MlvChartArea *self, GtkOrientation orientation, GtkScrollablePolicy policy)
{
	self->priv->scroll_policy[orientation] = policy;
	gtk_widget_queue_resize (GTK_WIDGET (self));
}

/**
 * mlv_chart_area_new:
 * @parser: TODO
 * @hadjustment: TODO
 * @vadjustment: TODO
 *
 * TODO
 *
 * Return value: (transfer full): TODO
 */
GtkWidget *
mlv_chart_area_new (MlvLogParser *parser, GtkAdjustment *hadjustment, GtkAdjustment *vadjustment)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (parser), NULL);
	g_return_val_if_fail (hadjustment == NULL || GTK_IS_ADJUSTMENT (hadjustment), NULL);
	g_return_val_if_fail (vadjustment == NULL || GTK_IS_ADJUSTMENT (vadjustment), NULL);

	return GTK_WIDGET (g_object_new (MLV_TYPE_CHART_AREA,
	                                 "parser", parser,
	                                 "hadjustment", hadjustment,
	                                 "vadjustment", vadjustment,
	                                 NULL));
}

/**
 * mlv_chart_area_get_hadjustment:
 * @self: a #MlvChartArea
 *
 * TODO
 *
 * Return value: TODO
 */
GtkAdjustment *
mlv_chart_area_get_hadjustment (MlvChartArea *self)
{
	g_return_val_if_fail (MLV_IS_CHART_AREA (self), NULL);

	return self->priv->adjustment[GTK_ORIENTATION_HORIZONTAL];
}

/**
 * mlv_chart_area_get_vadjustment:
 * @self: a #MlvChartArea
 *
 * TODO
 *
 * Return value: TODO
 */
GtkAdjustment *
mlv_chart_area_get_vadjustment (MlvChartArea *self)
{
	g_return_val_if_fail (MLV_IS_CHART_AREA (self), NULL);

	return self->priv->adjustment[GTK_ORIENTATION_VERTICAL];
}
