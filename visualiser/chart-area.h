/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MLV_CHART_AREA_H
#define MLV_CHART_AREA_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "log-parser.h"

G_BEGIN_DECLS

#define MLV_TYPE_CHART_AREA		(mlv_chart_area_get_type ())
#define MLV_CHART_AREA(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), MLV_TYPE_CHART_AREA, MlvChartArea))
#define MLV_CHART_AREA_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), MLV_TYPE_CHART_AREA, MlvChartAreaClass))
#define MLV_IS_CHART_AREA(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), MLV_TYPE_CHART_AREA))
#define MLV_IS_CHART_AREA_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), MLV_TYPE_CHART_AREA))
#define MLV_CHART_AREA_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), MLV_TYPE_CHART_AREA, MlvChartAreaClass))

typedef struct _MlvChartAreaPrivate	MlvChartAreaPrivate;

typedef struct {
	GtkWidget parent;
	MlvChartAreaPrivate *priv;
} MlvChartArea;

typedef struct {
	GtkWidgetClass parent;
} MlvChartAreaClass;

GType mlv_chart_area_get_type (void) G_GNUC_CONST;

GtkWidget *mlv_chart_area_new (MlvLogParser *parser, GtkAdjustment *hadjustment, GtkAdjustment *vadjustment) G_GNUC_WARN_UNUSED_RESULT G_GNUC_MALLOC;

GtkAdjustment *mlv_chart_area_get_hadjustment (MlvChartArea *self) G_GNUC_PURE;
GtkAdjustment *mlv_chart_area_get_vadjustment (MlvChartArea *self) G_GNUC_PURE;

G_END_DECLS

#endif /* !MLV_CHART_AREA_H */
