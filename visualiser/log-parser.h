/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MLV_LOG_PARSER_H
#define MLV_LOG_PARSER_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define MLV_TYPE_LOG_PARSER		(mlv_log_parser_get_type ())
#define MLV_LOG_PARSER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), MLV_TYPE_LOG_PARSER, MlvLogParser))
#define MLV_LOG_PARSER_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), MLV_TYPE_LOG_PARSER, MlvLogParserClass))
#define MLV_IS_LOG_PARSER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), MLV_TYPE_LOG_PARSER))
#define MLV_IS_LOG_PARSER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), MLV_TYPE_LOG_PARSER))
#define MLV_LOG_PARSER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), MLV_TYPE_LOG_PARSER, MlvLogParserClass))

typedef struct _MlvLogParserPrivate	MlvLogParserPrivate;

typedef struct {
	GObject parent;
	MlvLogParserPrivate *priv;
} MlvLogParser;

typedef struct {
	GObjectClass parent;
} MlvLogParserClass;

GType mlv_log_parser_get_type (void) G_GNUC_CONST;

MlvLogParser *mlv_log_parser_new (void) G_GNUC_WARN_UNUSED_RESULT G_GNUC_MALLOC;

void mlv_log_parser_parse (MlvLogParser *self, GFile *log_file, GCancellable *cancellable, GAsyncReadyCallback callback, gpointer user_data);
void mlv_log_parser_parse_finish (MlvLogParser *self, GAsyncResult *async_result, GError **error);

GArray *mlv_log_parser_get_threads (MlvLogParser *self) G_GNUC_PURE;
gint64 mlv_log_parser_get_min_timestamp (MlvLogParser *self) G_GNUC_PURE;
gint64 mlv_log_parser_get_max_timestamp (MlvLogParser *self) G_GNUC_PURE;
GSequence/*<MlvLogEvent>*/ *mlv_log_parser_get_events (MlvLogParser *self) G_GNUC_PURE;

/**
 * TODO
 */
typedef struct {
	GThread *thread_ptr; /* as returned by G_THREAD_SELF */
	GPid thread_pid; /* as returned by gettid() */
} MlvLogThread;

/**
 * TODO
 */
typedef enum {
	MLV_LOG_EVENT_MAIN_CONTEXT_NEW = 0,
	MLV_LOG_EVENT_MAIN_CONTEXT_DEFAULT,
	MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE,
	MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE,
	MLV_LOG_EVENT_MAIN_LOOP_NEW,
	MLV_LOG_EVENT_IDLE_ADD,
	MLV_LOG_EVENT_IDLE_DISPATCH,
	MLV_LOG_EVENT_TIMEOUT_ADD,
	MLV_LOG_EVENT_TIMEOUT_DISPATCH,
	MLV_LOG_EVENT_MAIN_LOOP_QUIT,
	MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY,
	MLV_LOG_EVENT_SOURCE_ATTACH,
} MlvLogEventType;

/**
 * TODO
 */
typedef struct _MlvLogEvent MlvLogEvent;

struct _MlvLogEvent {
	MlvLogEventType event_type;
	gint64 timestamp;
	guint thread_id; /* 0-based index into threads array */
	struct _MlvLogEvent *prev;
	struct _MlvLogEvent *next;
};

/**
 * TODO
 */
typedef struct {
	gpointer ptr;
	gchar *label;
} MlvAnnotatedPointer;

#define MLV_ANNOTATED_POINTER_STRUCT(T) \
	struct { \
		T *ptr; \
		gchar *label; \
	}

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
} MlvLogEventMainContextNew;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
} MlvLogEventMainContextDefault;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	GThread *thread;
} MlvLogEventMainContextAcquire;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	GThread *thread;
} MlvLogEventMainContextRelease;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainLoop *main_loop;
	GMainContext *context;
} MlvLogEventMainLoopNew;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	guint priority;
	MLV_ANNOTATED_POINTER_STRUCT(void) func;
	gpointer user_data;
	GSource *source;
	guint source_id;
} MlvLogEventIdleAdd;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	GMainLoop *main_loop;
	MLV_ANNOTATED_POINTER_STRUCT(void) func;
	gpointer user_data;
	gboolean again;
} MlvLogEventIdleDispatch;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	guint priority;
	guint timeout;
	MLV_ANNOTATED_POINTER_STRUCT(void) func;
	gpointer user_data;
	GSource *source;
	guint source_id;
} MlvLogEventTimeoutAdd;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
	GMainLoop *main_loop;
	MLV_ANNOTATED_POINTER_STRUCT(void) func;
	gpointer user_data;
} MlvLogEventTimeoutDispatch;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainLoop *main_loop;
} MlvLogEventMainLoopQuit;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GMainContext *context;
} MlvLogEventMainContextDestroy;

/**
 * TODO
 */
typedef struct {
	MlvLogEvent parent;
	GSource *source;
	GMainContext *context;
	guint source_id;
} MlvLogEventSourceAttach;

G_END_DECLS

#endif /* !MLV_LOG_PARSER_H */
