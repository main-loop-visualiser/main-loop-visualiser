/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <glib.h>
#include <gio/gio.h>

#include "log-parser.h"

static void mlv_log_parser_dispose (GObject *object);
static void mlv_log_parser_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void mlv_log_parser_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);

struct _MlvLogParserPrivate {
	GFile *log_file;

	GArray/*<MlvLogThread>*/ *threads;
	GSequence/*<MlvLogEvent>*/ *events;

	gint64 min_timestamp;
	gint64 max_timestamp;
};

typedef enum {
	PROP_LOG_FILE = 1
} MlvLogParserProperties;

typedef enum {
	SIGNAL_PARSE_COMPLETE = 1,
} MlvLogParserSignals;
#define NUM_SIGNALS 1

static guint signals[NUM_SIGNALS] = { 0, };

G_DEFINE_TYPE (MlvLogParser, mlv_log_parser, G_TYPE_OBJECT)

static void
mlv_log_parser_class_init (MlvLogParserClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MlvLogParserPrivate));

	gobject_class->get_property = mlv_log_parser_get_property;
	gobject_class->set_property = mlv_log_parser_set_property;
	gobject_class->dispose = mlv_log_parser_dispose;

	/**
	 * MlvLogParser:log-file:
	 *
	 * TODO
	 */
	g_object_class_install_property (gobject_class, PROP_LOG_FILE,
	                                 g_param_spec_object ("log-file",
	                                                      "Log file", "The log file to parse.",
	                                                      G_TYPE_FILE,
	                                                      G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

	/**
	 * MlvLogParser::parse-complete:
	 *
	 * TODO
	 */
	signals[SIGNAL_PARSE_COMPLETE] = g_signal_new ("parse-complete", G_OBJECT_CLASS_TYPE (gobject_class), G_SIGNAL_RUN_LAST, 0,
	                                               NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

static void
mlv_log_parser_init (MlvLogParser *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, MLV_TYPE_LOG_PARSER, MlvLogParserPrivate);
}

static void
mlv_log_parser_dispose (GObject *object)
{
	MlvLogParserPrivate *priv = MLV_LOG_PARSER (object)->priv;

	g_clear_object (&priv->log_file);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (mlv_log_parser_parent_class)->dispose (object);
}

static void
mlv_log_parser_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
	MlvLogParserPrivate *priv = MLV_LOG_PARSER (object)->priv;

	switch (property_id) {
		case PROP_LOG_FILE:
			g_value_set_object (value, priv->log_file);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
mlv_log_parser_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	MlvLogParserPrivate *priv = MLV_LOG_PARSER (object)->priv;

	switch (property_id) {
		case PROP_LOG_FILE:
			/* Construct only */
			priv->log_file = g_value_dup_object (value);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/**
 * mlv_log_parser_new:
 *
 * TODO
 *
 * Return value: (transfer full): TODO
 */
MlvLogParser *
mlv_log_parser_new (void)
{
	return MLV_LOG_PARSER (g_object_new (MLV_TYPE_LOG_PARSER, NULL));
}

static void
skip_whitespace (const gchar **line)
{
	while (**line == ' ' || **line == '\t') {
		*line = *line + 1;
	}
}

/* Partially parse a line of the form:
 *    0xdeadbeef (label), blah blah
 * or
 *    0xdeadbeef, blah blah
 * or
 *    0xdeadbeef
 *
 * Strip off the first field ("0xdeadbeef (label)" or "0xdeadbeef") and advance
 * the line pointer to the start of the next field.
 */
static gboolean
extract_pointer_field (const gchar **line, gpointer *pointer_output, gchar **label_output)
{
	const gchar *field_end = NULL;

	g_return_val_if_fail (line != NULL && *line != NULL, FALSE);
	g_return_val_if_fail (pointer_output != NULL, FALSE);

	/* Parse the pointer. */
	*pointer_output = (gpointer) g_ascii_strtoull (*line, (gchar**) &field_end, 16);

	if (field_end == NULL || field_end == *line) {
		/* Error! */
		goto error;
	}

	*line = field_end;
	skip_whitespace (line);

	/* If there's a label, parse it. */
	if (**line == '(') {
		gchar *close_bracket;

		*line = *line + 1; /* consume the opening bracket */
		close_bracket = strchr (*line, ')');

		if (close_bracket == NULL || close_bracket == *line) {
			/* Error! */
			goto error;
		}

		if (label_output != NULL) {
			*label_output = g_strndup (*line, close_bracket - *line);
		}

		*line = close_bracket + 1; /* consume the closing bracket */
		skip_whitespace (line);
	}

	/* Consume any trailing comma. */
	if (**line == ',') {
		*line = *line + 1; /* consume the comma */
		skip_whitespace (line);
	}

	return TRUE;

error:
	/* Clean up. */
	*pointer_output = NULL;
	if (label_output != NULL) {
		g_free (*label_output);
		*label_output = NULL;
	}

	return FALSE;
}

static gboolean
extract_unsigned_integer_field (const gchar **line, guint *output)
{
	const gchar *field_end = NULL;

	/* Parse the integer. */
	*output = g_ascii_strtoull (*line, (gchar**) &field_end, 10);

	if (field_end == NULL || field_end == *line) {
		/* Error! */
		return FALSE;
	}

	*line = field_end;
	skip_whitespace (line);

	/* Consume any trailing comma. */
	if (**line == ',') {
		*line = *line + 1; /* consume the comma */
		skip_whitespace (line);
	}

	return TRUE;
}

static gboolean
extract_boolean_field (const gchar **line, gboolean *output)
{
	guint int_output;

	if (extract_unsigned_integer_field (line, &int_output) == FALSE ||
	    (int_output != 0 && int_output != 1)) {
		return FALSE;
	}

	*output = (int_output == 1) ? TRUE : FALSE;

	return TRUE;
}

/* Format: 0xdeadbeef */
static gboolean
parse_main_context_new (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainContextNew *evt = (MlvLogEventMainContextNew*) event;

	/* GMainContext pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef */
static gboolean
parse_main_context_default (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainContextDefault *evt = (MlvLogEventMainContextDefault*) event;

	/* GMainContext pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbeefdead */
static gboolean
parse_main_context_acquire (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainContextAcquire *evt = (MlvLogEventMainContextAcquire*) event;

	/* GMainContext pointer, GThread pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->thread, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbeefdead */
static gboolean
parse_main_context_release (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainContextRelease *evt = (MlvLogEventMainContextRelease*) event;

	/* GMainContext pointer, GThread pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->thread, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbeefdead */
static gboolean
parse_main_loop_new (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainLoopNew *evt = (MlvLogEventMainLoopNew*) event;

	/* GMainContext pointer, GMainLoop pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->main_loop, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 5, 0xaaaaaaaa, 0xbeefdead */
static gboolean
parse_idle_add (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventIdleAdd *evt = (MlvLogEventIdleAdd*) event;

	/* GMainContext pointer, priority, function pointer, user data pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_unsigned_integer_field (line, &evt->priority) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->func.ptr, &evt->func.label) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->user_data, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->source, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->source_id, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbbbbbbbb, 0xaaaaaaaa (label), 0xbeefdead, 0|1 */
static gboolean
parse_idle_dispatch (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventIdleDispatch *evt = (MlvLogEventIdleDispatch*) event;

	/* GMainContext pointer, GMainLoop pointer, function pointer, user data pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->main_loop, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->func.ptr, &evt->func.label) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->user_data, NULL) == FALSE ||
	    extract_boolean_field (line, &evt->again) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 5, 30, 0xaaaaaaaa, 0xbeefdead */
static gboolean
parse_timeout_add (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventTimeoutAdd *evt = (MlvLogEventTimeoutAdd*) event;

	/* GMainContext pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_unsigned_integer_field (line, &evt->priority) == FALSE ||
	    extract_unsigned_integer_field (line, &evt->timeout) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->func.ptr, &evt->func.label) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->user_data, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->source, NULL) == FALSE ||
	    extract_unsigned_integer_field (line, &evt->source_id) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbbbbbbbb, 0xaaaaaaaa, 0xbeefdead */
static gboolean
parse_timeout_dispatch (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventTimeoutDispatch *evt = (MlvLogEventTimeoutDispatch*) event;

	/* GMainContext pointer, GMainLoop pointer, function pointer, user data pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->main_loop, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->func.ptr, &evt->func.label) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->user_data, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef */
static gboolean
parse_main_loop_quit (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainLoopQuit *evt = (MlvLogEventMainLoopQuit*) event;

	/* GMainLoop pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->main_loop, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef */
static gboolean
parse_main_context_destroy (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventMainContextDestroy *evt = (MlvLogEventMainContextDestroy*) event;

	/* GMainContext pointer. */
	if (extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

/* Format: 0xdeadbeef, 0xbeefdead, 15 */
static gboolean
parse_source_attach (MlvLogEvent *event, const gchar **line)
{
	MlvLogEventSourceAttach *evt = (MlvLogEventSourceAttach*) event;

	/* GSource pointer, GMainContext pointer, source ID. */
	if (extract_pointer_field (line, (gpointer*) &evt->source, NULL) == FALSE ||
	    extract_pointer_field (line, (gpointer*) &evt->context, NULL) == FALSE ||
	    extract_unsigned_integer_field (line, &evt->source_id) == FALSE) {
		/* Error! */
		return FALSE;
	}

	return TRUE;
}

typedef struct {
	const gchar *event_type_string;
	MlvLogEventType event_type;
	gsize struct_size;
	gboolean (*parser_func) (MlvLogEvent *event, const gchar **line);
} EventTypeData;

/* NOTE: This must be indexed by MlvLogEventType. */
static EventTypeData event_type_data[] = {
	{ "g_main_context_new", MLV_LOG_EVENT_MAIN_CONTEXT_NEW, sizeof (MlvLogEventMainContextNew), parse_main_context_new },
	{ "g_main_context_default", MLV_LOG_EVENT_MAIN_CONTEXT_DEFAULT, sizeof (MlvLogEventMainContextDefault), parse_main_context_default },
	{ "g_main_context_acquire", MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE, sizeof (MlvLogEventMainContextAcquire), parse_main_context_acquire },
	{ "g_main_context_release", MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE, sizeof (MlvLogEventMainContextRelease), parse_main_context_release },
	{ "g_main_loop_new", MLV_LOG_EVENT_MAIN_LOOP_NEW, sizeof (MlvLogEventMainLoopNew), parse_main_loop_new },
	{ "g_idle_add", MLV_LOG_EVENT_IDLE_ADD, sizeof (MlvLogEventIdleAdd), parse_idle_add },
	{ "g_idle_dispatch", MLV_LOG_EVENT_IDLE_DISPATCH, sizeof (MlvLogEventIdleDispatch), parse_idle_dispatch },
	{ "g_timeout_add", MLV_LOG_EVENT_TIMEOUT_ADD, sizeof (MlvLogEventTimeoutAdd), parse_timeout_add },
	{ "g_timeout_dispatch", MLV_LOG_EVENT_TIMEOUT_DISPATCH, sizeof (MlvLogEventTimeoutDispatch), parse_timeout_dispatch },
	{ "g_main_loop_quit", MLV_LOG_EVENT_MAIN_LOOP_QUIT, sizeof (MlvLogEventMainLoopQuit), parse_main_loop_quit },
	{ "g_main_context_destroy", MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY, sizeof (MlvLogEventMainContextDestroy),
	  parse_main_context_destroy },
	{ "g_source_attach", MLV_LOG_EVENT_SOURCE_ATTACH, sizeof (MlvLogEventSourceAttach), parse_source_attach },
};

static gint
event_sort_cb (const MlvLogEvent *event_a, const MlvLogEvent *event_b, gpointer user_data)
{
	return event_a->timestamp - event_b->timestamp;
}

static void
add_event (MlvLogParser *self, MlvLogEvent *event)
{
	MlvLogParserPrivate *priv = self->priv;

	/* Add the event. */
	g_sequence_insert_sorted (priv->events, event, (GCompareDataFunc) event_sort_cb, NULL);

	/* Update metadata. */
	if (priv->min_timestamp > event->timestamp) {
		priv->min_timestamp = event->timestamp;
	}

	if (priv->max_timestamp < event->timestamp) {
		priv->max_timestamp = event->timestamp;
	}
}

static void
log_event_free (MlvLogEvent *event)
{
	g_slice_free1 (event_type_data[event->event_type].struct_size, event);
}

static void
parse_thread (GSimpleAsyncResult *async_result, MlvLogParser *self, GCancellable *cancellable)
{
	MlvLogParserPrivate *priv = self->priv;
	GFileInputStream *input_stream;
	GDataInputStream *data_input_stream = NULL;
	gchar *line = NULL;
	gsize line_length = 0;
	guint line_number = 0;
	GSequenceIter *events_iter, *events_begin_iter;
	GHashTable *unpaired_main_context_events;
	GError *child_error = NULL;

	/* TODO: Check memory and error handling in this function. */

	/* Open the file. */
	/* TODO: MlvLogParser needs to be thread safe */
	input_stream = g_file_read (priv->log_file, cancellable, &child_error);

	if (child_error != NULL) {
		goto done;
	}

	/* Create a data input stream wrapper to implement buffering and line-by-line reading. */
	data_input_stream = g_data_input_stream_new (G_INPUT_STREAM (input_stream));

	/* Parse it line by line. */
	while ((line = g_data_input_stream_read_line_utf8 (data_input_stream, &line_length, cancellable, &child_error)) != NULL) {
		const gchar *line_i, *line_j;
		gint64 timestamp;
		GPid thread_pid; /* as returned by gettid() */
		guint thread_id; /* index into priv->threads */
		MlvLogEvent *event_data = NULL;
		guint i;

		line_number++;

		/* Comment or empty line? */
		if (line_length == 0 || line[0] == '#') {
			goto next;
		}

		line_i = line;

		/* Extract the timestamp. */
		line_j = line_i;
		timestamp = g_ascii_strtoll (line_i, (gchar**) &line_j, 10);

		if (line_j == NULL || line_j == line_i || *line_j != ',') {
			/* Error! */
			goto line_error;
		}

		line_i = line_j + 1 /* comma */;
		skip_whitespace (&line_i);

		/* Extract the thread ID. */
		if (strncmp (line_i, "Thread ", strlen ("Thread ")) != 0) {
			/* Error! */
			goto line_error;
		}

		line_i += strlen ("Thread ");

		line_j = line_i;
		thread_pid = g_ascii_strtoull (line_i, (gchar**) &line_j, 10);

		if (line_j == NULL || line_j == line_i || *line_j != ',') {
			/* Error! */
			goto line_error;
		}

		line_i = line_j + 1 /* comma */;
		skip_whitespace (&line_i);

		/* Extract the event type. */
		for (i = 0; i < G_N_ELEMENTS (event_type_data); i++) {
			if (strncmp (line_i, event_type_data[i].event_type_string,
			             strlen (event_type_data[i].event_type_string)) == 0) {
				/* We've got the right value of i. Let's build the event data. */
				event_data = (MlvLogEvent*) g_slice_alloc (event_type_data[i].struct_size);
				event_data->event_type = event_type_data[i].event_type;
				break;
			}
		}

		if (i == G_N_ELEMENTS (event_type_data) || *(line_i + strlen (event_type_data[i].event_type_string)) != ',') {
			/* Error! */
			goto line_error;
		}

		line_i += strlen (event_type_data[i].event_type_string) + 1 /* comma */;
		skip_whitespace (&line_i);

		/* Extract the parameters. */
		if (event_type_data[i].parser_func (event_data, &line_i) == FALSE || *line_i != '\0') {
			/* Error! */
			goto line_error;
		}

		/* Add a new thread if necessary. */
		for (i = 0; i < priv->threads->len; i++) {
			if (g_array_index (priv->threads, MlvLogThread, i).thread_pid == thread_pid) {
				thread_id = i;
				break;
			}
		}

		if (i == priv->threads->len) {
			MlvLogThread new_thread;

			new_thread.thread_ptr = NULL; /* TODO */
			new_thread.thread_pid = thread_pid;

			g_array_append_val (priv->threads, new_thread);
			thread_id = priv->threads->len - 1;
		}

		/* Save the event data. */
		event_data->timestamp = timestamp;
		event_data->thread_id = thread_id;
		event_data->next = NULL;
		event_data->prev = NULL;

		add_event (self, event_data);
		event_data = NULL;

		goto next;

line_error:
		if (event_data != NULL) {
			g_slice_free1 (event_type_data[i].struct_size, event_data);
			event_data = NULL;
		}

		g_warning ("Skipping line %u due to a parsing error.", line_number);

next:
		g_free (line);
		line = NULL;
		line_i = line_j = NULL;
		line_length = 0;
		timestamp = 0;
		thread_id = 0;

		continue;
	}

	if (child_error != NULL) {
		goto done;
	}

	/* Post-process the events list to build chains of related events. For example, corresponding acquisitions and releases
	 * of a given GMainContext will be linked together. The processing is performed in reverse order. */
	events_iter = g_sequence_get_end_iter (priv->events);
	events_begin_iter = g_sequence_get_begin_iter (priv->events);

	unpaired_main_context_events = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL, NULL);

	do {
		MlvLogEvent *event, *next_event;

		/* Get the previous iter. We do this first because the end iter isn't valid for calls to g_sequence_get(), whereas the begin
		 * iter is.
		 *
		 * The possible chains are (given in pseudo-regexp syntax):
		 *   MAIN_CONTEXT_NEW -> MAIN_CONTEXT_DEFAULT? -> (MAIN_CONTEXT_ACQUIRE -> MAIN_CONTEXT_RELEASE)* -> MAIN_CONTEXT_DESTROY
		 */
		events_iter = g_sequence_iter_prev (events_iter);

		event = (MlvLogEvent*) g_sequence_get (events_iter);

		switch (event->event_type) {
			case MLV_LOG_EVENT_MAIN_CONTEXT_NEW:
				next_event = g_hash_table_lookup (unpaired_main_context_events, ((MlvLogEventMainContextNew*) event)->context);

				if (next_event != NULL &&
				    (next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE ||
				     next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_DEFAULT ||
				     next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY)) {
					/* Link the events. */
					g_assert (next_event->prev == NULL);
					g_assert (event->next == NULL);

					next_event->prev = event;
					event->next = next_event;
				}

				g_hash_table_remove (unpaired_main_context_events, ((MlvLogEventMainContextNew*) event)->context);

				break;
			case MLV_LOG_EVENT_MAIN_CONTEXT_DEFAULT:
				next_event = g_hash_table_lookup (unpaired_main_context_events, ((MlvLogEventMainContextDefault*) event)->context);

				if (next_event != NULL &&
				    (next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE ||
				     next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY)) {
					/* Link the events. */
					g_assert (next_event->prev == NULL);
					g_assert (event->next == NULL);

					next_event->prev = event;
					event->next = next_event;
				}

				g_hash_table_replace (unpaired_main_context_events, ((MlvLogEventMainContextDefault*) event)->context, event);

				break;
			case MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE:
				next_event = g_hash_table_lookup (unpaired_main_context_events, ((MlvLogEventMainContextAcquire*) event)->context);

				if (next_event != NULL &&
				    (next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE ||
				     next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY)) {
					/* Link the events. */
					g_assert (next_event->prev == NULL);
					g_assert (event->next == NULL);

					next_event->prev = event;
					event->next = next_event;
				}

				g_hash_table_replace (unpaired_main_context_events, ((MlvLogEventMainContextAcquire*) event)->context, event);

				break;
			case MLV_LOG_EVENT_MAIN_CONTEXT_RELEASE:
				next_event = g_hash_table_lookup (unpaired_main_context_events, ((MlvLogEventMainContextRelease*) event)->context);

				if (next_event != NULL &&
				    (next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_ACQUIRE ||
				     next_event->event_type == MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY)) {
					/* Link the events. */
					g_assert (next_event->prev == NULL);
					g_assert (event->next == NULL);

					next_event->prev = event;
					event->next = next_event;
				}

				g_hash_table_replace (unpaired_main_context_events, ((MlvLogEventMainContextRelease*) event)->context, event);

				break;
			case MLV_LOG_EVENT_MAIN_CONTEXT_DESTROY:
				/* Start the chain. */
				g_hash_table_replace (unpaired_main_context_events, ((MlvLogEventMainContextDestroy*) event)->context, event);

				break;
			case MLV_LOG_EVENT_MAIN_LOOP_NEW:
			case MLV_LOG_EVENT_IDLE_ADD:
			case MLV_LOG_EVENT_IDLE_DISPATCH:
			case MLV_LOG_EVENT_TIMEOUT_ADD:
			case MLV_LOG_EVENT_TIMEOUT_DISPATCH:
			case MLV_LOG_EVENT_MAIN_LOOP_QUIT:
			case MLV_LOG_EVENT_SOURCE_ATTACH:
				/* TODO */
				break;
			default:
				g_assert_not_reached ();
		}
	} while (events_iter != events_begin_iter);

	g_hash_table_unref (unpaired_main_context_events);

done:
	/* Tidy up. */
	g_free (line);
	g_clear_object (&data_input_stream);

	if (input_stream != NULL) {
		g_input_stream_close (G_INPUT_STREAM (input_stream), NULL, NULL);
	}

	/* Report any error to the main thread. */
	if (child_error != NULL) {
		g_simple_async_result_take_error (async_result, child_error);
	}

	g_clear_object (&input_stream);
}

/* TODO: Make parser forwards-compatible by ignoring extra fields at the end of lines */

/**
 * mlv_log_parser_parse:
 * @self: a #MlvLogParser
 * @log_file: TODO
 * @cancellable: (allow-none): TODO
 * @callback: TODO
 * @user_data: TODO
 *
 * TODO
 */
void
mlv_log_parser_parse (MlvLogParser *self, GFile *log_file, GCancellable *cancellable, GAsyncReadyCallback callback, gpointer user_data)
{
	MlvLogParserPrivate *priv;
	GSimpleAsyncResult *async_result;

	g_return_if_fail (MLV_IS_LOG_PARSER (self));
	g_return_if_fail (G_IS_FILE (log_file));
	g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));
	g_return_if_fail (callback != NULL);

	priv = self->priv;

	/* Reset the data. */
	if (priv->events != NULL) {
		g_sequence_free (priv->events);
	}
	priv->events = g_sequence_new ((GDestroyNotify) log_event_free);

	if (priv->threads != NULL) {
		g_array_unref (priv->threads);
	}
	priv->threads = g_array_new (FALSE, FALSE, sizeof (MlvLogThread));

	priv->min_timestamp = G_MAXINT64;
	priv->max_timestamp = G_MININT64;

	g_clear_object (&priv->log_file);

	priv->log_file = g_object_ref (log_file);

	/* Start the parsing thread. */
	async_result = g_simple_async_result_new (G_OBJECT (self), callback, user_data, mlv_log_parser_parse);
	g_simple_async_result_run_in_thread (async_result, (GSimpleAsyncThreadFunc) parse_thread, G_PRIORITY_DEFAULT, cancellable);
	g_object_unref (async_result);
}

/**
 * mlv_log_parser_parse_finish:
 * @self: a #MlvLogParser
 * @async_result: TODO
 * @error: TODO
 *
 * TODO
 */
void
mlv_log_parser_parse_finish (MlvLogParser *self, GAsyncResult *async_result, GError **error)
{
	g_return_if_fail (MLV_IS_LOG_PARSER (self));
	g_return_if_fail (G_IS_ASYNC_RESULT (async_result));
	g_return_if_fail (error == NULL || *error == NULL);

	g_return_if_fail (g_simple_async_result_is_valid (async_result, G_OBJECT (self), mlv_log_parser_parse));

	/* Error? */
	if (g_simple_async_result_propagate_error (G_SIMPLE_ASYNC_RESULT (async_result), error) == TRUE) {
		return;
	}

	/* TODO: signal stuff? */
	g_signal_emit (self, signals[SIGNAL_PARSE_COMPLETE], 0);
}

/**
 * mlv_log_parser_get_threads:
 * @self: a #MlvLogParser
 *
 * TODO
 *
 * Return value: (element-type MlvLogThread) (transfer none): TODO NOTE: the array is of the structs themselves, not pointers to the structs
 */
GArray *
mlv_log_parser_get_threads (MlvLogParser *self)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (self), NULL);

	/* TODO: notify when this changes */
	return self->priv->threads;
}

/**
 * mlv_log_parser_get_min_timestamp:
 * @self: a #MlvLogParser
 *
 * TODO
 *
 * Return value: TODO
 */
gint64
mlv_log_parser_get_min_timestamp (MlvLogParser *self)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (self), 0);

	return self->priv->min_timestamp;
}

/**
 * mlv_log_parser_get_max_timestamp:
 * @self: a #MlvLogParser
 *
 * TODO
 *
 * Return value: TODO
 */
gint64
mlv_log_parser_get_max_timestamp (MlvLogParser *self)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (self), 0);

	return self->priv->max_timestamp;
}

/**
 * mlv_log_parser_get_events:
 * @self: a #MlvLogParser
 *
 * TODO
 *
 * Return value: (element-type MlvLogEvent) (transfer none): TODO
 */
GSequence/*<MlvLogEvent>*/ *
mlv_log_parser_get_events (MlvLogParser *self)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (self), NULL);

	return self->priv->events;
}
