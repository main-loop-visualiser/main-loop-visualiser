/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "main-window.h"
#include "chart-area.h"

static void mlv_main_window_constructed (GObject *object);
static void mlv_main_window_dispose (GObject *object);
static void mlv_main_window_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void mlv_main_window_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);

static void save_window_state (MlvMainWindow *self);
static void restore_window_state (MlvMainWindow *self);

static gboolean mw_delete_event_cb (GtkWindow *window, gpointer user_data);

struct _MlvMainWindowPrivate {
	MlvLogParser *parser;
};

typedef enum {
	PROP_PARSER = 1,
} MlvMainWindowProperties;

G_DEFINE_TYPE (MlvMainWindow, mlv_main_window, GTK_TYPE_WINDOW)
#define MLV_MAIN_WINDOW_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MLV_TYPE_MAIN_WINDOW, MlvMainWindowPrivate))

static void
mlv_main_window_class_init (MlvMainWindowClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MlvMainWindowPrivate));

	gobject_class->constructed = mlv_main_window_constructed;
	gobject_class->dispose = mlv_main_window_dispose;
	gobject_class->get_property = mlv_main_window_get_property;
	gobject_class->set_property = mlv_main_window_set_property;

	/**
	 * MlvChartArea:parser:
	 *
	 * TODO
	 */
	g_object_class_install_property (gobject_class, PROP_PARSER,
	                                 g_param_spec_object ("parser",
	                                                      "Parser", "Log file parser TODO.",
	                                                      MLV_TYPE_LOG_PARSER,
	                                                      G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
mlv_main_window_init (MlvMainWindow *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, MLV_TYPE_MAIN_WINDOW, MlvMainWindowPrivate);

	gtk_window_set_title (GTK_WINDOW (self), _("Main Loop Visualiser"));

	g_signal_connect (self, "delete-event", G_CALLBACK (mw_delete_event_cb), NULL);
}

static void
mlv_main_window_constructed (GObject *object)
{
	MlvMainWindow *self = MLV_MAIN_WINDOW (object);
	GtkWidget *scrolled_window, *child_widget;

	/* Create our child widgets. */
	scrolled_window = gtk_scrolled_window_new (NULL, NULL);

	child_widget = mlv_chart_area_new (self->priv->parser,
	                                   gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (scrolled_window)),
	                                   gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (scrolled_window)));
	gtk_container_add (GTK_CONTAINER (scrolled_window), child_widget);
	gtk_widget_show (child_widget);

	gtk_container_add (GTK_CONTAINER (self), scrolled_window);
	gtk_widget_show (scrolled_window);

	restore_window_state (self);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (mlv_main_window_parent_class)->constructed (object);
}

static void
mlv_main_window_dispose (GObject *object)
{
	MlvMainWindowPrivate *priv = MLV_MAIN_WINDOW (object)->priv;

	g_clear_object (&priv->parser);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (mlv_main_window_parent_class)->dispose (object);
}

static void
mlv_main_window_get_property (GObject *object, MlvMainWindowProperties property_id, GValue *value, GParamSpec *pspec)
{
	MlvMainWindowPrivate *priv = MLV_MAIN_WINDOW (object)->priv;

	switch (property_id) {
		case PROP_PARSER:
			g_value_set_object (value, priv->parser);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
mlv_main_window_set_property (GObject *object, MlvMainWindowProperties property_id, const GValue *value, GParamSpec *pspec)
{
	MlvMainWindowPrivate *priv = MLV_MAIN_WINDOW (object)->priv;

	switch (property_id) {
		case PROP_PARSER:
			/* Construct only. */
			g_assert (priv->parser == NULL);
			priv->parser = g_value_dup_object (value);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/**
 * mlv_main_window_new:
 * @parser: TODO
 *
 * TODO
 *
 * Return value: (transfer full): TODO
 */
MlvMainWindow *
mlv_main_window_new (MlvLogParser *parser)
{
	g_return_val_if_fail (MLV_IS_LOG_PARSER (parser), NULL);

	return MLV_MAIN_WINDOW (g_object_new (MLV_TYPE_MAIN_WINDOW,
	                                      "parser", parser,
	                                      NULL));
}

static GFile *
get_window_state_file (void)
{
	GFile *key_file_path;
	gchar *filename;

	filename = g_build_filename (g_get_user_config_dir (), PACKAGE_NAME, "state.ini", NULL);
	key_file_path = g_file_new_for_path (filename);
	g_free (filename);

	return key_file_path;
}

static void
save_window_state (MlvMainWindow *self)
{
	GKeyFile *key_file;
	GFile *key_file_path = NULL, *key_file_directory;
	gchar *key_file_data;
	gsize key_file_length;
	GdkWindow *window;
	GdkWindowState state;
	gint width, height, x, y;
	GError *error = NULL;

	/* Overwrite the existing state file with a new one */
	key_file = g_key_file_new ();

	window = gtk_widget_get_window (GTK_WIDGET (self));
	state = gdk_window_get_state (window);

	/* Maximisation state */
	g_key_file_set_boolean (key_file, "main-window", "maximized", state & GDK_WINDOW_STATE_MAXIMIZED ? TRUE : FALSE);

	/* Save the window dimensions */
	gtk_window_get_size (GTK_WINDOW (self), &width, &height);

	g_key_file_set_integer (key_file, "main-window", "width", width);
	g_key_file_set_integer (key_file, "main-window", "height", height);

	/* Save the window position */
	gtk_window_get_position (GTK_WINDOW (self), &x, &y);

	g_key_file_set_integer (key_file, "main-window", "x-position", x);
	g_key_file_set_integer (key_file, "main-window", "y-position", y);

	/* Serialise the key file data */
	key_file_data = g_key_file_to_data (key_file, &key_file_length, &error);
	g_key_file_free (key_file);

	if (error != NULL) {
		g_warning ("Error generating window state data: %s", error->message);
		g_error_free (error);
		goto done;
	}

	/* Ensure that the correct directories exist */
	key_file_path = get_window_state_file ();

	key_file_directory = g_file_get_parent (key_file_path);
	g_file_make_directory_with_parents (key_file_directory, NULL, &error);
	g_object_unref (key_file_directory);

	if (error != NULL) {
		if (error->code != G_IO_ERROR_EXISTS) {
			gchar *parse_name = g_file_get_parse_name (key_file_path);
			g_warning ("Error creating directory for window state data file “%s”: %s", parse_name, error->message);
			g_free (parse_name);
		}

		g_clear_error (&error);

		/* Continue to attempt to write the file anyway */
	}

	/* Save the new key file (synchronously, since we want it to complete before we finish quitting the program) */
	g_file_replace_contents (key_file_path, key_file_data, key_file_length,
	                         NULL, FALSE, G_FILE_CREATE_PRIVATE, NULL, NULL, &error);

	if (error != NULL) {
		gchar *parse_name = g_file_get_parse_name (key_file_path);
		g_warning ("Error saving window state data to “%s”: %s", parse_name, error->message);
		g_free (parse_name);

		g_error_free (error);
		goto done;
	}

done:
	if (key_file_path != NULL) {
		g_object_unref (key_file_path);
	}

	g_free (key_file_data);
}

static void
restore_window_state_cb (GFile *key_file_path, GAsyncResult *result, MlvMainWindow *self)
{
	GKeyFile *key_file = NULL;
	gchar *key_file_data = NULL;
	gsize key_file_length;
	gint width = -1, height = -1, x = -1, y = -1;
	GError *error = NULL;

	g_file_load_contents_finish (key_file_path, result, &key_file_data, &key_file_length, NULL, &error);

	if (error != NULL) {
		if (error->code != G_IO_ERROR_NOT_FOUND) {
			gchar *parse_name = g_file_get_parse_name (key_file_path);
			g_warning ("Error loading window state data from “%s”: %s", parse_name, error->message);
			g_free (parse_name);
		}

		g_error_free (error);
		goto done;
	}

	/* Skip loading the key file if it has zero length */
	if (key_file_length == 0) {
		goto done;
	}

	/* Load the key file's data into the GKeyFile */
	key_file = g_key_file_new ();
	g_key_file_load_from_data (key_file, key_file_data, key_file_length, G_KEY_FILE_NONE, &error);

	if (error != NULL) {
		gchar *parse_name = g_file_get_parse_name (key_file_path);
		g_warning ("Error loading window state data from “%s”: %s", parse_name, error->message);
		g_free (parse_name);

		g_error_free (error);
		goto done;
	}

	/* Load the appropriate keys from the file, ignoring errors */
	width = g_key_file_get_integer (key_file, "main-window", "width", NULL);
	height = g_key_file_get_integer (key_file, "main-window", "height", NULL);
	x = g_key_file_get_integer (key_file, "main-window", "x-position", &error);

	if (error != NULL) {
		x = -1;
		g_clear_error (&error);
	}

	y = g_key_file_get_integer (key_file, "main-window", "y-position", &error);

	if (error != NULL) {
		x = -1;
		g_clear_error (&error);
	}

	/* Make sure the dimensions and position are sane */
	if (width > 1 && height > 1) {
		GdkScreen *screen;
		gint max_width, max_height;

		screen = gtk_widget_get_screen (GTK_WIDGET (self));
		max_width = gdk_screen_get_width (screen);
		max_height = gdk_screen_get_height (screen);

		width = CLAMP (width, 0, max_width);
		height = CLAMP (height, 0, max_height);

		x = CLAMP (x, 0, max_width - width);
		y = CLAMP (y, 0, max_height - height);

		gtk_window_resize (GTK_WINDOW (self), width, height);
	}

	if (x >= 0 && y >= 0) {
		gtk_window_move (GTK_WINDOW (self), x, y);
	}

	/* Maximised? */
	if (g_key_file_get_boolean (key_file, "main-window", "maximized", NULL) == TRUE) {
		gtk_window_maximize (GTK_WINDOW (self));
	}

done:
	g_free (key_file_data);

	if (key_file != NULL) {
		g_key_file_free (key_file);
	}
}

static void
restore_window_state (MlvMainWindow *self)
{
	GFile *key_file_path;

	/* Asynchronously load up the state key file */
	key_file_path = get_window_state_file ();
	g_file_load_contents_async (key_file_path, NULL, (GAsyncReadyCallback) restore_window_state_cb, self);
	g_object_unref (key_file_path);
}

static gboolean
mw_delete_event_cb (GtkWindow *window, gpointer user_data)
{
	save_window_state (MLV_MAIN_WINDOW (window));

	gtk_widget_destroy (GTK_WIDGET (window));

	return TRUE;
}
