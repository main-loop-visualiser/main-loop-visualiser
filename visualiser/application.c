/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "application.h"
#include "main-window.h"

static void constructed (GObject *object);
static void dispose (GObject *object);

static void activate (GApplication *application);
static void open (GApplication *application, GFile **files, gint n_files, const gchar *hint);

struct _MlvApplicationPrivate {
	MlvLogParser *parser;
	MlvMainWindow *main_window;
};

G_DEFINE_TYPE (MlvApplication, mlv_application, GTK_TYPE_APPLICATION)

static void
mlv_application_class_init (MlvApplicationClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GApplicationClass *gapplication_class = G_APPLICATION_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MlvApplicationPrivate));

	gobject_class->constructed = constructed;
	gobject_class->dispose = dispose;

	gapplication_class->activate = activate;
	gapplication_class->open = open;
}

static void
mlv_application_init (MlvApplication *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, MLV_TYPE_APPLICATION, MlvApplicationPrivate);

	/* Create the parser. */
	self->priv->parser = mlv_log_parser_new ();
}

static void
constructed (GObject *object)
{
	/* Set various properties up */
	g_application_set_application_id (G_APPLICATION (object), "org.gnome.MainLoopVisualiser");
	g_application_set_flags (G_APPLICATION (object), G_APPLICATION_HANDLES_OPEN);

	/* Localisation */
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	g_set_application_name (_("Main Loop Visualiser"));
	gtk_window_set_default_icon_name ("main-loop-visualiser");

	/* Chain up to the parent class */
	G_OBJECT_CLASS (mlv_application_parent_class)->constructed (object);
}

static void
dispose (GObject *object)
{
	MlvApplicationPrivate *priv = MLV_APPLICATION (object)->priv;

	if (priv->main_window != NULL) {
		gtk_widget_destroy (GTK_WIDGET (priv->main_window));
		priv->main_window = NULL;
	}

	g_clear_object (&priv->parser);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (mlv_application_parent_class)->dispose (object);
}

/* Nullify our pointer to the main window when it gets destroyed (e.g. when we quit) so that we don't then try
 * to destroy it again in dispose(). */
static void
main_window_destroy_cb (MlvMainWindow *main_window, MlvApplication *self)
{
	self->priv->main_window = NULL;
}

static void
activate (GApplication *application)
{
	MlvApplication *self = MLV_APPLICATION (application);
	MlvApplicationPrivate *priv = self->priv;

	/* Create the interface */
	if (priv->main_window == NULL) {
		priv->main_window = mlv_main_window_new (priv->parser);
		gtk_window_set_application (GTK_WINDOW (priv->main_window), GTK_APPLICATION (application));
		gtk_widget_show (GTK_WIDGET (priv->main_window));
		g_signal_connect (priv->main_window, "destroy", (GCallback) main_window_destroy_cb, application);
	}

	/* Bring it to the foreground */
	gtk_window_present (GTK_WINDOW (priv->main_window));
}

static void
open_cb (MlvLogParser *parser, GAsyncResult *async_result, MlvApplication *self)
{
	GError *child_error = NULL;

	/* Finish the parse operation. */
	mlv_log_parser_parse_finish (parser, async_result, &child_error);

	if (child_error != NULL) {
		/* TODO */
		g_error ("Error opening file: %s", child_error->message);
		g_error_free (child_error);
	}

	/* Create the window. TODO */
	activate (G_APPLICATION (self));

	g_application_release (G_APPLICATION (self));
}

static void
open (GApplication *application, GFile **files, gint n_files, const gchar *hint)
{
	MlvApplication *self = MLV_APPLICATION (application);
	MlvApplicationPrivate *priv = self->priv;

	/* TODO: How about one window per file? */
	if (n_files == 1) {
		g_application_hold (application);
		mlv_log_parser_parse (priv->parser, files[0], NULL, (GAsyncReadyCallback) open_cb, application);
		/* We release the application in the callback. */
	} else {
		g_warning ("Can only handle one file from the command line.");
	}
}

MlvApplication *
mlv_application_new (void)
{
	return MLV_APPLICATION (g_object_new (MLV_TYPE_APPLICATION, NULL));
}
