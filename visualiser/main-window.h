/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Main Loop Visualiser
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MLV_MAIN_WINDOW_H
#define MLV_MAIN_WINDOW_H

#include <config.h>
#include <glib.h>
#include <glib-object.h>

#include "log-parser.h"

G_BEGIN_DECLS

#define MLV_TYPE_MAIN_WINDOW		(mlv_main_window_get_type ())
#define MLV_MAIN_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), MLV_TYPE_MAIN_WINDOW, MlvMainWindow))
#define MLV_MAIN_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), MLV_TYPE_MAIN_WINDOW, MlvMainWindowClass))
#define MLV_IS_MAIN_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), MLV_TYPE_MAIN_WINDOW))
#define MLV_IS_MAIN_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), MLV_TYPE_MAIN_WINDOW))
#define MLV_MAIN_WINDOW_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), MLV_TYPE_MAIN_WINDOW, MlvMainWindowClass))

typedef struct _MlvMainWindowPrivate	MlvMainWindowPrivate;

typedef struct {
	GtkWindow parent;
	MlvMainWindowPrivate *priv;
} MlvMainWindow;

typedef struct {
	GtkWindowClass parent;
} MlvMainWindowClass;

GType mlv_main_window_get_type (void) G_GNUC_CONST;

MlvMainWindow *mlv_main_window_new (MlvLogParser *parser) G_GNUC_WARN_UNUSED_RESULT G_GNUC_MALLOC;

G_END_DECLS

#endif /* !MLV_MAIN_WINDOW_H */
